﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToDoListInterviewProject.Controllers
{
    public class HomeController : Controller
    {
        //define the context
        IContext context = new SessionContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTaskList()
        {
            return PartialView("_TaskList", context.Tasks);
        }

        public ActionResult Edit(int? id, bool value)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            if (!context.Update(id.Value, value))
            {
                return HttpNotFound();
            }
            else
            {
                context.Update(id.Value, value);
                return PartialView("_TaskList", context.Tasks);
            }
            
        }

        public ActionResult Delete(int Id)
        {
            context.Delete(Id);

            return PartialView("_TaskList", context.Tasks);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTask([Bind(Include = "Description")] Task task)
        {                        
            if (ModelState.IsValid)
            {
                context.Add(task);                 
            }

            return PartialView("_TaskList", context.Tasks);
        }
    }
}