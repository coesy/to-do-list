﻿$(document).ready(function () {
    $('.CompletedCheck').change(function () {
        var item = $(this);
        var id = item.attr('id');
        var val = item.prop('checked');

        $.ajax({
            url: '/Home/Edit',
            data: {
                id: id,
                value: val
            },
            type: 'POST',
            success: function (result) {
                $('#divTaskList').html(result);
            }
        })
    })
})

function UpdateResults(result)
{
    $('#divTaskList').html(result); 
}