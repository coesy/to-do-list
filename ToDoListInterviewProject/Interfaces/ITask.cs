﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListInterviewProject
{
    //Allows for any type of Task to be created

    public interface ITask
    {
        int? Id { get; set; }
        bool Completed { get; set; }
        string Description { get; set; }
    }
}
