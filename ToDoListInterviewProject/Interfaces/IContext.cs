﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListInterviewProject
{
    //PC This could be a session context or a DbContext

    /// <summary>
    /// Interface to allow for any type of container to save to
    /// </summary>
    public interface IContext
    {
        List<ITask> Tasks {get;}

        List<ITask> GetTasks();

        int Add(ITask task);
        bool Delete(int Id);
        bool Update(int Id, bool Complete);

        void SaveChanges();
    }
}
