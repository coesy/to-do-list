﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListInterviewProject
{
    public class SessionContext : IContext
    {
        
        public List<ITask> Tasks { get; set; }


        public SessionContext()
        {
            GetTasks(); 
        }

        public List<ITask> GetTasks()
        {
            if (HttpContext.Current.Session["Tasks"] == null)
                Tasks = new List<ITask>();
            else
                Tasks = (List<ITask>)HttpContext.Current.Session["Tasks"];

            return Tasks;
        }

        public bool Update(int id, bool complete)
        {
            Tasks.Find(it => it.Id == id).Completed = complete;
            SaveChanges();
            return true;            
        }

        public bool Delete(int id)
        {
            Tasks.Remove(Tasks.Find(it => it.Id == id));
            SaveChanges();
            return true;
        }

        public int Add(ITask task)
        {
            if (task.Id == null)
            {
                if (Tasks == null || Tasks.Count == 0)
                    task.Id = 1;
                else
                    task.Id = Tasks.Max(t => t.Id) + 1;
            }

            Tasks.Add(task);
            SaveChanges();
            return task.Id.Value;
        }

        public void SaveChanges()
        {
            System.Web.HttpContext.Current.Session["Tasks"] = Tasks;
        }
    }
}